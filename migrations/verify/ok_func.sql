-- Verify migrations:ok_func on pg

BEGIN;

SELECT has_function_privilege('public.ok_func()', 'execute');

ROLLBACK;
