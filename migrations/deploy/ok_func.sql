-- Deploy migrations:ok_func to pg

BEGIN;


CREATE OR REPLACE FUNCTION public.ok_func(OUT ok_func JSON) AS $$
BEGIN
    SELECT row_to_json(aux)
    INTO ok_func
    FROM (
             SELECT
                 'ok'                                                                                                                                                                                                      status,
                 st_asgeojson(
                     '01030000000100000005000000010000E8238821C049C444B9B5B14440010000C0008821C049C444B9B5B14440010000C0008821C0780DA53DBDB14440010000E8238821C0780DA53DBDB14440010000E8238821C049C444B9B5B14440') :: JSON geo_json
         ) aux;

END $$ LANGUAGE plpgsql;

COMMIT;
