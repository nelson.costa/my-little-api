## Dependencies
run `ansible-galaxy install -r ansible/roles.yml -p ansible/galaxy`

## Database
```
ssh -f -L 54321:localhost:5432 vagrant@192.168.0.10 -N
psql -h localhost -p 54321 -U postgres database
```

## Sqitch
[Sqitch tutorial](https://metacpan.org/pod/sqitchtutorial)